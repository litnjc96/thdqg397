% Copyright (c) 2014,2016 Casper Ti. Vector
% Public domain.

\chapter{基于微服务的应用模型规约}

软件系统正在变得越来越复杂，导致软件复杂度不断上升的一个重要原因是软件运行环境及软件自身所具有的动态性和不确定因素的持续增长，在基于微服务的应用的语境下，这种复杂性从设计、开发、部署阶段一直延伸到后开发阶段（维护、演化等）。为了让软件在环境变化后仍能持续满足用户需求，本文提出了一种基于微服务的应用模型，通过将模型驱动工程中模型的应用从开发阶段扩展到运行阶段，利用软件模型对运行时丰富和不确定的信息进行管理。通过使用模型，软件开发者可以忽略具体的实现细节，在高层次的抽象上规约和设计逻辑，从而避免了将系统应对变化的逻辑与程序语言交织带来的复杂性，简化了应用系统的设计、开发、部署和演化过程。

\section{问题的提出}

\subsection{微服务应用资源调度场景}

基于微服务的应用被部署在动态变化的云环境中，同时需要满足动态变化、不可预测的用户需求。在这种应用体系结构中，组件（即微服务）众多、相互协同配合完成特定的业务功能，为了持续、高质量地提供服务，各个微服务需要能够在运行时检测环境变化和自身状态，并据此对自身进行调整；同时各个微服务“集成”为一个整体的应用，动态调整的过程需要从全局进行系统化的分析、决策和执行。


\subsection{微服务依赖关系}

微服务将复杂的软件系统拆分成功能单一、可独立开发部署的服务组件，通过轻量级通信机制使得这些组件协同配合，从而形成一种高内聚低耦合的系统体系结构，在这种体系结构下，微服务的组件众多，依赖关系复杂，软件更新频繁。

轻量级的通信机制例如基于HTTP协议的RESTful风格API，将业务领域对象抽象为资源作为基本的接口单元，例如一个提供用户信息的微服务，可以通过如下接口访问特定用户的信息：

GET /v1/users/1

返回结果为：

\begin{lstlisting}[language=json]
{
  "first": "Ruici",
  "last": "Luo",
  "email": "luoruici@pku.edu.cn",
  "bookmarks": [ "12345", "12346", "12347" ],
  "total_view_time": 10800
}
\end{lstlisting}

bookmarks此时只是一个包含书签id的列表，如果要进行展开，则需要继续请求：

GET /v1/users/1/bookmarks

\begin{lstlisting}[language=json]
[
  {
    "id": "12345",
    "title": "Doctor Strange",
    "description": "...",
    "poster": "//cdn.tubitv.com/12345.jpg",
    "url": "//cdn.tubitv.com/12345-1280x714-,559,1414,2236,2642,3628,k.mp4.m3u8"
  },
  ...
]
\end{lstlisting}

然后客户端根据这些数据，实现自己的业务功能。RESTful风格的API非常简洁、功能明确，但是对于客户端的调用非常不友好，多次请求以实现同一个业务功能，对于基于微服务的复杂应用来说性能会受到巨大的影响。现阶段存在一些折中的解决方案，例如将请求合并使得客户端只发送一次请求，但是这仍然无法满足客户端灵活的需求。这些方案基本上都是对现有的方法进行“补丁升级”，各自为战，缺乏整体的观念，容易互相影响，并且对微服务乃至应用整体的可维护性造成巨大的伤害。

为了解决上述问题，本文对微服务提供的数据模型进行更细粒度的建模，同时定义了一种针对数据模型的查询语言，通过该查询语言可以灵活、高效地访问微服务提供的数据以实现业务功能，同时数据模型和查询语言也是对微服务之间的关系实现的高层指导，例如以下查询：

\begin{lstlisting}[language=json]
{
  user(id: 1) {
    first
    last
    bookmarks {
      id
      title
      description
    }
  }
}
\end{lstlisting}

能够获取包含bookmark数据的用户对象数据，而以下查询：

\begin{lstlisting}[language=json]
{
  user(id: 1) {
    first
    last
  }
}
\end{lstlisting}

则仅仅获得用户的基本信息。

\section{基于微服务的应用元模型规约}

基于微服务的应用体系结构模型分为三层：

\begin{itemize}
	\item 拓扑结构层：描述应用的逻辑拓扑结构，包括组成应用的微服务本身的抽象和微服务之间依赖关系的抽象
	\item 基础设施层：描述微服务的运行时信息，包括微服务运行时的环境（容器、虚拟机）、系统软件依赖（操作系统、中间件等）、运行时数据（资源使用情况、负载等）
	\item 实例层：描述微服务的伸缩和更新过程
\end{itemize}


下图描述了了体系结构模型：

\includegraphics[width=6in]{images/app-model.png}

\subsubsection{拓扑结构层}

Application表示一个基于微服务的应用，它由一系列微服务——Service组成，每一个微服务对外暴露一系列端点——Entrypoint用于提供服务。

同时模型中引入版本的概念，用于捕获应用的演化过程特征。应用版本——ApplicationVersion描述了演化过程中应用的特定状态；相应地，组成应用的每一个微服务在演化过程中也拥有一些列版本——ServiceVersion，一个微服务版本会包含端点版本——EntryPointVersion集合。版本的概念刻画了应用运行环境中，随着时间的推移存在着不同的微服务运行实例。每一个微服务实例的EntrypointVersion集合，表示当前微服务实例对外提供的业务功能接口，EntrypointVersion可以认为是EntryPoint在演化过程中特定时间点下的一个特化实例。

因此，应用在某一时刻的状态ApplicationVersion，包含着多个不同的微服务版本ServiceVersion，为了实现在线演化（保证应用模型发生变化和功能调整期间仍然能够持续提供服务），应用可能包含同一个微服务的多个不同版本；可以认为应用的演化过程可以描述为随着时间推移所得到的ApplicationVersion序列。

\subsubsection{基础设施层}

模型的基础设施层用于刻画环境、依赖和运行时数据等信息：

\begin{itemize}
	\item Provider：基础设施服务提供商，例如Google, AWS, Azure等等，一个应用可以运行在在多个Provider上。
	\item Location：为了保证可用性，Provider会提供多个可用区——Location为用户提供服务，每一个Location的内容对于用户来说是完全一样，可以将Application部署与不同给的Location上提高安全性和可用性。
	\item Host：ServiceReplica的运行单元——例如容器、虚拟机、物理机。Host上可以运行一个或多个ServiceReplica（可以属于同一个ServiceVersion，也可以属于不同ServiceVersion）。
	\item Metric：在开放网络环境中、提供持续服务的大规模分布式软件应用系统的背景下，运行环境和资源的变化为系统演化提供了重要的依据。Metric是对Host的运行时信息的描述，包含CPU, 内存， 网络带宽，响应时间等数据。景，\end{itemize}

\subsubsection{实例层}

在运行时存在对应同一个ServiceVersion的多个运行实例ServiceReplica。ServiceReplica属于某一个Host。

Message是描述服务之间的功能调用的核心概念，Message关联起始ServiceReplica和目标ServiceReplica，表示起始服务到目标服务的一次调用。同时Message还会关联目标ServiceReplica所述的ServiceVersion中的特定EntrypointVersion来表示具体的调用地址（例如REST API的入口）

\section{基于微服务的应用调度模型规约}



\section{描述语言？}
